import {Injectable} from '@angular/core';

import {Job} from '../model/job';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Http, Response, RequestOptions, URLSearchParams} from '@angular/http';


@Injectable()
export class ApicallsService {
  google_api_key = '';
  career_jet_url = '/jobs';
  places_url = '/googleplaces';
  matrix_url = '/distancematrix';


  constructor(public http: Http) {
  }

  // make a http call to careerjet public API and return and return an observable
  getCareerJetResults(search, location): Observable<Job[]> {
  }

  // make a http call to google's distance  matrix API and return and return an observable
  getDistance(address1, address2): Observable<any> {
  }

  // make a http call to google's places  matrix API and return and return an observable
  getAddressLatLng(address): Observable<any> {
  }

}
