import {Component, OnInit} from '@angular/core';
import {JobSetGetService} from '../../service/job-set-get.service';
import {Job} from '../../model/job';
import {Router} from '@angular/router';
import {ApicallsService} from '../../service/apicalls.service';

declare var google: any;

@Component({
  selector: 'app-map-page',
  templateUrl: './map-page.component.html',
  styleUrls: ['./map-page.component.css']
})
export class MapPageComponent implements OnInit {
  job: Job;
  user_location: any;
  error_message: string;

  constructor(public jobget: JobSetGetService, public router: Router, public apiProvider: ApicallsService) {
  }

  // Initialize Components
  ngOnInit() {

  }
  /* Using the distance matrix in API Service Call, get the distance between current's user's location and job location. */
  getDistanceLocation() {

  }

  /*Check to see if the data is empty, if it is render a map without any marker, if not,
     Based on google's api documentation, set the options of the maps*/
  setMap() {

  }

}
