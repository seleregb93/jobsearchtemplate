import { JobSearchPage } from './app.po';

describe('job-search App', function() {
  let page: JobSearchPage;

  beforeEach(() => {
    page = new JobSearchPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
